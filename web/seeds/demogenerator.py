from flask_seeder import Seeder, Faker, generator
from models import Product


# All seeders inherit from Seeder
class DemoSeeder(Seeder):

    # run() will be called by Flask-Seeder
    def run(self):
        id_gen_pattern = "\d[a-z][a-z]\d[a-z][a-z]\d[a-z][a-z]\d[a-z][a-z]\d[a-z][a-z]\d[a-z][a-z]\d[a-z][a-z]\d[a-z][a-z]"
        # Create a new Faker and tell it how to create Product objects
        faker = Faker(
          cls=Product,
          init={
            "id": generator.String(id_gen_pattern),
            "name": generator.String('Товар [a-z][a-z][a-z]'),
          }
        )

        # Create 25 products
        for prod in faker.create(25):
            print("Adding product: %s" % prod)
            self.db.session.add(prod)
