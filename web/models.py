# models.py


import datetime
from app import db
import uuid
from sqlalchemy.orm import relationship


def pkgen():
    return str(lambda: uuid.uuid4().hex)[0:36]


class Product(db.Model):

    __tablename__ = 'product'

    id = db.Column(db.String, primary_key=True, index=True)
    name = db.Column(db.String, unique=False, nullable=False)

    def __repr__(self):
        return self.name


class OrderItem(db.Model):

    __tablename__ = 'order items'

    ord_line_id = db.Column(db.String, primary_key=True)
    order_line_line_no = db.Column(db.Integer, nullable=False)
    order_id = db.Column(db.String, db.ForeignKey('orders.id'), nullable=True)
    product_id = db.Column(db.String, db.ForeignKey('product.id'), nullable=True)
    product_name = relationship("Product", backref='product', lazy="joined")
    product_price = db.Column(db.Float, nullable=False)
    amount = db.Column(db.Integer, nullable=False)

    def __init__(self, order_line_line_no, order_id, product_id, product_price, amount):
        self.ord_line_id = str(uuid.uuid4().hex)[0:36]
        self.order_line_line_no = order_line_line_no
        self.order_id = order_id
        self.product_id = product_id
        self.product_price = product_price
        self.amount = amount


class Order(db.Model):

    __tablename__ = 'orders'

    id = db.Column(db.String, primary_key=True, index=True)
    created_date = db.Column(db.DateTime, nullable=False)
    number = db.Column(db.String, unique=False, nullable=True)
    order_lines = db.relationship('OrderItem', backref='order items', lazy=True)
    order_sum = db.Column(db.Float, nullable=True)

    def __repr__(self):
        return self.number+' '+str(self.created_date)+'(id: '+self.id+')'

    def get_pk_value(self):
        return self.id

    def __init__(self, number, date):
        if not self.id:
            self.id = str(uuid.uuid4().hex)[0:36]
            print('init new pk: ' + self.id + ' for Order')
            self.number = str(number)
            if date:
                self.created_date = date
            else:
                self.created_date = datetime.datetime.now()
