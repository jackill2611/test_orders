# app.py


from flask import Flask
from flask import request, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import and_, func
from config import BaseConfig
from flask_migrate import Migrate
from flask_seeder import FlaskSeeder
import random


app = Flask(__name__)
app.config.from_object(BaseConfig)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
# seeder for generating demo data
seeder = FlaskSeeder()
seeder.init_app(app, db)


from models import *


@app.route('/api/management/<int:orders_qty>', methods=['GET'])
def create_orders(orders_qty):
    # Create [orders_qty] orders
    generated_orders = []
    start_date = datetime.date(2018, 1, 1)
    qty_of_products = Product.query.count()
    if qty_of_products == 0:
        warning_text = 'can\'c generate orders because there are no products in a base!'
        print(warning_text)
        return render_template('warning.html', warning_text=warning_text)
    print('qty of products in this base: '+str(qty_of_products))
    for n in range(1, orders_qty+1):
        ordr = Order(n, start_date + datetime.timedelta(days=n-1))
        print("Adding orders: %s" % ordr)
        db.session.add(ordr)
        generated_orders.append(ordr)
        # generate random 1..5 order lines
        random.seed()
        no_of_orderlines = random.randint(1, 5)
        for lineno in range(1, no_of_orderlines+1):
            random_product_id = Product.query.all()[random.randint(0, qty_of_products-1)].id
            ordrline = OrderItem(lineno, ordr.id, random_product_id, random.randint(100, 999), random.randint(1, 10))
            db.session.add(ordrline)

    db.session.commit()
    return render_template('orders.html', orders=generated_orders, orders_qty=str(orders_qty))


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/orders', methods=['GET'])
def orderslist():
    orders = Order.query.order_by(Order.created_date.desc()).all()
    return render_template('orders.html', orders=orders)


@app.route('/orderlines', methods=['GET'])
def orderlineslist():
    orderitems = OrderItem.query.order_by(OrderItem.order_id.desc()).all()
    return render_template('orderlines.html', orderitems=orderitems)


@app.route('/products', methods=['GET'])
def productlist():
    products = Product.query.all()
    return render_template('products.html', products=products)


@app.route('/report1', methods=['POST', 'GET'])
def report():
    error = None
    if request.method == 'POST':
        startdate = request.form['startdate']
        enddate = request.form['enddate']
        print(str(startdate)+'  '+str(enddate))
        if startdate > enddate:
            error = 'Incorrect interval: start date should be equal or greater than end date!'
            return render_template('report1.html', error=error)
        else:
            reportdata = db.session.query(Order).filter(and_(db.func.date(Order.created_date) >= startdate, db.func.date(Order.created_date) <= enddate)).all()
            data_array = []
            report_total = 0
            for ord in reportdata:
                order_sum = 0
                for ord_item in ord.order_lines:
                    order_sum += ord_item.product_price*ord_item.amount
                    report_total += ord_item.product_price*ord_item.amount
                data_array.append([ord, order_sum])

            return render_template('report1.html', startdate=startdate, enddate=enddate, reportdata=data_array, report_total=report_total)
    else:
        error = 'Something gone wrong. Try again'
        return render_template('report1.html', error=error)


@app.route('/report2', methods=['GET'])
def report2():
    error = None
    # aggregate sum of quantity of each product
    data = db.session.query(OrderItem.product_id, func.sum(OrderItem.amount).label('sum')).group_by(OrderItem.product_id).subquery()
    # TOP-100 records of products that are the most sellable
    res = db.session.query(OrderItem, data.c.sum).join(data, OrderItem.product_id == data.c.product_id).order_by(-data.c.sum).limit(100)
    data_array = []
    for orderline, amount in res:
        v_order = Order.query.filter(Order.id == orderline.order_id).first()
        data_array.append([v_order.number, v_order.created_date, orderline.product_name, orderline.product_price])

    return render_template('report2.html', reportdata=data_array)


if __name__ == '__main__':
    app.run()
