This is a test task, so there is only an instruction how to deploy this app and check how it works.

System requirements :

    Linux Ubuntu or Windows 10.

Docker deployment:

    Install Docker (docker-compose (https://docs.docker.com/compose/install/) and docker-machine (https://docs.docker.com/machine/install-machine/))
    
    Clone the project from this repository to your local folder.
    
    Run terminal (Term1) from this folder
    
    Run next commands:
    
        (Term1)
        docker-machine create -d virtualbox dev
        (Term1)
        docker-compose build
        (Term1)
        docker-compose up -d
        (Term1)
        docker-compose run web /usr/local/bin/python create_db.py
        
    open another terminal (Term2) from this folder and find out testorders_web docker container ID:
        (Term2)
        docker container list
        
    now, with container ID that you have got, enter this' container bash
        (Term2)
        docker exec -it <container id> bash
        
    and now we are going to generate some demo-data for our app
        (Term2)
        flask seed run
    
    and last command in Terminal 1
    
        (Term1)
        docker-compose up -d
    
    you can work in application via browser, just enter http://localhost in your browser's address bar
    
    
    
Local deployment:

    -Install Anaconda 3.7 (https://www.anaconda.com/distribution/)
    
    -Install Postgresql
    
    -Clone the project from this repository to your local folder.
    
    -Create database in postgres (let''s call in testdb)
    
    -Modify file config.py (copypaste this):
    
        # config.py


        import os
    
        class BaseConfig(object):
        SECRET_KEY = 5(15ds+i2+%ik6z&!yer+ga9m=e%jcqiz_5wszg)r-z!2--b2d
        DEBUG = FALSE
        DB_NAME = testdb
        DB_USER = [your postgres user]
        DB_PASS = [your postgres user password]
        DB_SERVICE = [your postgres server ip or hostname]
        DB_PORT = [your postgres server port]
        SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(
            DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME
        )
        
    -Create conda virtual env for this app
        
        Run in terminal:
        
        conda create -n testenv python=3.7
        
    -next, activate your testenv environment:
    
        conda activate testenv
        
    -install required libs:
    
        pip install -r requirements.txt
        
    -run Flask create db file:
    
        python create_db.py
        
    -run creator of demo-data:
    
        flask seed run
        
    -run the app:
    
        flask run
        
    -go to http://localhost:8000 in your browser

        